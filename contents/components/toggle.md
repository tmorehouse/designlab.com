---
name: Toggle
related:
  - checkboxes
  - radio-button
  - segmented-control
---

A toggle is a visual switch which acts as a boolean.

## Usage

Use toggles when there is a choice between two selections, it has a default state, the results are effective and noticeable immediately, and there is **no need** for a user to click a submit button.

Avoid using toggles when a submit button is needed. This confuses users and dilutes the experience by preventing instant results.

Toggles may replace a segmented control, two radio buttons, or a single checkbox to allow users to choose between two opposing states. For help with choosing the right solution, follow the table below.

Todo: Add replacement-comparison-table

### Labels

Toggle labels are set in bold, positioned above the element by default, and should describe what the control will do when the switch is on in a concise, direct, and short way.

In rare cases and exceptions, the label can be placed to the left of the input field. Examples include non inline positioned elements.

### Visual Design

Toggles should use high-contrast colors to indicate the states - On and Off.

## Demo

Todo: Add live component block with code example

## Design specifications

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[Sketch Measure Preview for toggles](https://gitlab-org.gitlab.io/gitlab-design/hosted/design-gitlab-specs/toggles-spec-previews/)
