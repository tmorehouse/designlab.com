---
name: The GitLab logo
---

The GitLab logo consists of two components, the icon (the tanuki) and the wordmark:

![GitLab logo lockup](/img/brand/gitlab-lockup.png)

GitLab is most commonly represented by the logo, and in some cases, the icon alone. GitLab is rarely represented by the wordmark alone as we'd like to build brand recognition of the icon alone (e.g. the Nike swoosh), and doing so by pairing it with the GitLab wordmark.

## Logo safe space

Safe space acts as a buffer between the logo or icon and other visual components, including text. this space is the minimum distance needed and is equal to the x-height of the GitLab wordmark:

![GitLab logo x-height](/img/brand/x-height.png) ![GitLab logo safe space](/img/brand/logo-safe-space.png) ![GitLab logo icon safe space](/img/brand/icon-safe-space.png)

The x-height also determines the proper spacing between icon and workdmark, as well as, the correct scale of the icon relative to the wordmark:

![stacked GitLab logo safe space](/img/brand/stacked-logo-safe-space.png)

## The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [mission](https://about.gitlab.com/company/strategy/#mission) that everyone can contribute, our [values](https://about.gitlab.com/handbook/values/), and our [open source stewardship](https://about.gitlab.com/company/stewardship/).

## GitLab trademarks & logo guidelines

GitLab is a registered trademark of GitLab, Inc. You are welcome to use the GitLab trademark and logo, subject to the terms of the [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The most current version of the GitLab logo can be found on our [Press page](https://about.gitlab.com/press/).

Under the Creative Commons license, you may use the GitLab trademark and logo so long as you give attribution to GitLab and provide a link to the license. If you make any changes to the logo, you must state so, along with the attribution, and distribute under the same license.

Your use of the GitLab trademark and logo:

* May not be for commercial purposes;
* May not suggest or imply that you or your use of the GitLab trademark or logo is endorsed by GitLab, or create confusion as to whether or not you or your use of the GitLab trademark or logo is endorsed by GitLab; and
* May not suggest or imply or that you are affiliated with GitLab in any way, or create confusion as to whether or not you are affiliated with GitLab in any way.

Examples of improper use of the GitLab trademark and logo:

* The GitLab name may not be used in any root URL, including subdomains such as `gitlab.company.com` or `gitlab.citool.io`.
* The GitLab trademark and/or logo may not be used as the primary or prominent feature on any non-GitLab materials.

## Using other logos

Logos used on the about.gitlab.com site should always be in full color and be used to the specifications provided by the owner of that logo, which can usually be found on the owners website. The trust marks component found throughout the site is the only exception and should use a neutral tone:

![other logos](/img/brand/trust-marks.png)

The tanuki logo should also not have facial features (eyes, ears, nose…), it is meant to be kept neutral, but it can be accessorized.
