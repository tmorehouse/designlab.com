---
name: Principles
---

Principles act as a reusable standard for teams to measure their work. They replace subjective ideals with a shared understanding of what the result must do for users. Just as guardrails keep you safe and on the road, principles keep teams on the path to achieving their vision. By achieving alignment, principles enable us to scale and build velocity.

There are two kinds of principles; The principles that guide the process and principles that define the output. The process principles are described in our [handbook](https://about.gitlab.com/handbook/product/#product-principles), while the outcome principles are detailed here.

Each principle has been assigned a hierarchy position to resolve confusion about what to do in a specific circumstance while remaining consistent.

We take inspiration from other companies though our principles are defined by looking inward. This helps to determine which are most actionable and effective. Just like the rest of our work, we continually adjust our principles and strive always to make them better. Our principle guidelines are defined at the bottom of this page, so everyone is welcome to suggest improvements by opening an issue and/or creating a merge request in our [repository](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com)!

## 1 Productive

GitLab is an application to support what people do, day in, day out. We need to respect the importance of their work and avoid gimicky details.

<!--
 1. **Bold sub principle(s) title** Explanation, plus optional reference
  - Example(s), plus link towards a reference in real-life
-->

## 2 Minimal

While work can get complicated, GitLab is about bringing a sharp focus to help our customers know what matters now.

<!--
 1. **Bold sub principle(s) title** Explanation, plus optional reference
  - Example(s), plus link towards a reference in real-life
-->

## 3 Human

We need to build empathy with our users. This means we must understand their state of mind and connect with them on a human level.

<!--
1. **Bold sub principle(s) title** Explanation, plus optional reference
  - Example(s), plus link towards a reference in real-life
-->

## Principle guidelines

The principles defined should align to the following guidelines to assure they are to be most effective.

Note: The current principles in place may not yet conform to these guidelines, but will be updated to do so in the near future.

### Origin and purpose

Company values and purpose should inform principles, while principles should either inform the process or the desired output. Make sure that no conflicting principles are introduced.

### Target audience

Principles provide guidance to creators, ensuring they are focusing on the right problems for their users. Different internal teams (such as product, design, engineering, and marketing) sometimes require different sets of principles to guide them towards a shared outcome for the product end user. Ensure you are using the correct principles based on the task that you are performing.

### Actionable and genuine

Principles should be actionable and thus strike a balance between being too generic and too specific. Additionally, for them to be genuine and have context, they are required to have real-life examples.

### Give direction by being opinionated

When principles have a point of view, they give direction and keep teams on the path to achieving their vision. It defines a decision which otherwise could have diverged. By contrast, it is less useful to state things in principles that are logical and wished upon by default. An example of this would be “Useful,” of which the opposite would be “not useful.” Chances are slim that a company does not want to create a useful product.

### Explicit hierarchy

Each principle should be assigned a hierarchy position to resolve confusion about what to do in a specific circumstance while remaining consistent.

### Memorable

Similar to the [GitLab Company Values](https://about.gitlab.com/handbook/values/#about-our-values), principles are most effective when they are easy to remember. Because of this, the number of principles should be kept low. Three to five principles are ideal. Sub-principles are acceptable so long as they are aiding in making the principle more memorable.

### Format

```
## Principle title (Number of hierarchy position)

Intro stating why, how, and what of the principle.

1. **Bold sub principle(s) title** Explanation, plus optional reference
  - Example(s), plus link towards a reference in real-life
```
    
## References

- [Design systems handbook - Design better](https://www.designbetter.co/design-systems-handbook/expanding-design-system)
- [From purpose to patterns - Alla Kholmatova](https://speakerdeck.com/craftui/from-purpose-to-patterns)
- [What are design principles - principles.design](https://principles.design/#what-are-design-principles)
